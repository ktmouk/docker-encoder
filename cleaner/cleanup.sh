#!/bin/bash

IFS_BACKUP=$IFS
IFS=$'\n'

: ${CLEANER_DIR:=./data}
: ${CLEANER_LIMIT:=60}
: ${CLEANER_WAIT_TIME:=60}

elapsed_time() {
  echo $(expr $(date +"%s") - $(date +"%s" -r "$1"))
}

print() {
  echo "[$(date)] $1"
}

cleanup_file() {
  for file in $(find "$CLEANER_DIR" -type f)
  do
    if [ $(elapsed_time "$file") -gt "$CLEANER_LIMIT" ]; then
      rm "$file" && print "[del] $file"
    fi
  done
}

cleanup_dir() {
  for dir in $(find "$CLEANER_DIR" -type d | sort -r)
  do
    print "[checking]"
    if [ $(elapsed_time "$dir") -gt "$CLEANER_LIMIT" ]; then
      rmdir "$dir" 2>/dev/null && print "[del] $dir"
    fi
  done
}

print "[started]"
while true
do
  cleanup_file
  cleanup_dir
  sleep $CLEANER_WAIT_TIME
done
print "[stopped]"

IFS=$IFS_BACKUP
