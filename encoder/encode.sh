#!/bin/bash

IFS_BACKUP=$IFS
IFS=$'\n'

: ${ENCODER_SRC_DIR:=/src}
: ${ENCODER_DST_DIR:=/dst}
: ${ENCODER_TMP_DIR:=/tmp}
: ${ENCODER_PRESET_FILE:=/libx264-hq-ts.ffpreset}
: ${ENCODER_WAIT_TIME:=60}

encode() {
  ffmpeg -i "$1" \
    -deinterlace \
    -vcodec libx264 \
    -crf 18 \
    -vf scale=-1:720 \
    -acodec aac -ab 128k -ar 48000 \
    "$(tmp_file $1)"

  if [ "$?" -eq 0 ]; then
    mkdir -p "$ENCODER_DST_DIR"
    mv "$(tmp_file $1)" "$(dst_file $1)"
    rm -rfv "$1"
  fi
}

tmp_file() {
  echo "$ENCODER_TMP_DIR"/"$(basename "$1")"
}

dst_file() {
  mp4_file="$(basename "$1" | sed 's/\.m2ts$/.mp4/')"
  echo "$ENCODER_DST_DIR"/"$mp4_file"
}

print() {
  echo "[$(date)] $1"
}

encode_all() {
  for file in $(find "$ENCODER_SRC_DIR" -name "*.m2ts" -type f)
  do
    if [ -e "$(tmp_file $file)" ]; then
      continue;
    fi

    print "[enc] $file"
    encode "$file"
    print "[end] $file"
  done
}

print "[started]"
while true
do
  print "[checking]"
  encode_all
  sleep $ENCODER_WAIT_TIME
done
print "[stopped]"

IFS=$IFS_BACKUP
